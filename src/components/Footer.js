import React, {Component} from "react";

class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
           <p className="container copyright"> Copyright © 2020 Robert Bissell.<span>  All rights reserved.</span></p>
        )
    }

}

export default Footer